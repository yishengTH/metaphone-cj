<div align="center">
<h1>metaphone-cj</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v1.0.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.54.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-100.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>

## <img alt="" src="./doc/readme-image/readme-icon-introduction.png" style="display: inline-block;" width=3%/> 1 介绍

### 1.1 项目特性

1. 语音算法，支持将一个特定的字符串（通常是一个英文单词），将其转化为一个代码，然后可以将其与其他代码（或其他单词）进行比较，以检查他们是否（可能）发音相同。
2. 本项目致敬[metaphone-js](https://github.com/words/metaphone)
3. 您只需在cjpm.toml中[dependencies]写入

```shell
metaphone_cj = { git = "https://gitcode.com/yishengTH/metaphone-cj.git", output-type = "static" }
```

​	  


### 1.2 项目计划

1. 2024 年 9 月发布 1.0.0 版本。

   后续可能会继续进行重构，支持更多的metaphone算法。

## <img alt="" src="./doc/readme-image/readme-icon-framework.png" style="display: inline-block;" width=3%/> 2 架构

### 2.1 项目结构

```shell
.
├── README.md
├── LICENSE
├── CHANGELOG
├── cjpm.toml
├── doc
|   └── readme-image
|
└── src
	└── test
    	├── metaphone_test.cj           # 测试代码
    └── metaphone.cj             # 核心代码
```

### 2.2 接口说明

1. public func metaphone(value: String): String

   输入一个字符串作为metaphone算法的input，输出处理后的结果

## <img alt="" src="./doc/readme-image/readme-icon-compile.png" style="display: inline-block;" width=3%/> 3 使用说明

### 3.1 编译构建（Win/Linux/Mac）

```shell
cjpm build
```

### 3.2 功能示例

#### 3.2.1 metaphone算法

```cangjie
import metaphone_cj.*

main() {
    let output = metaphone("appearance")
    println(output) // expected "APRNS"
}
```

规则集描述：

1. Drop duplicate adjacent letters, except for C.
2. If the word begins with 'KN', 'GN', 'PN', 'AE', 'WR', drop the first letter.
3. Drop 'B' if after 'M' and if it is at the end of the word.
4. 'C' transforms to 'X' if followed by 'IA' or 'H' (unless in latter case, it is part of '-SCH-', in which case it transforms to 'K'). 'C' transforms to 'S' if followed by 'I', 'E', or 'Y'. Otherwise, 'C' transforms to 'K'.
5. 'D' transforms to 'J' if followed by 'GE', 'GY', or 'GI'. Otherwise, 'D' transforms to 'T'.
6. Drop 'G' if followed by 'H' and 'H' is not at the end or before a vowel. Drop 'G' if followed by 'N' or 'NED' and is at the end.
7. 'G' transforms to 'J' if before 'I', 'E', or 'Y', and it is not in 'GG'. Otherwise, 'G' transforms to 'K'. Reduce 'GG' to 'G'.
8. Drop 'H' if after vowel and not before a vowel.
9. 'CK' transforms to 'K'.
10. 'PH' transforms to 'F'.
11. 'Q' transforms to 'K'.
12. 'S' transforms to 'X' if followed by 'H', 'IO', or 'IA'.
13. 'T' transforms to 'X' if followed by 'IA' or 'IO'. 'TH' transforms to '0'. Drop 'T' if followed by 'CH'.
14. 'V' transforms to 'F'.
15. 'WH' transforms to 'W' if at the beginning. Drop 'W' if not followed by a vowel.
16. 'X' transforms to 'S' if at the beginning. Otherwise, 'X' transforms to 'KS'.
17. Drop 'Y' if not followed by a vowel.
18. 'Z' transforms to 'S'.
19. Drop all vowels unless it is the beginning.

注意：如果输入非字母，会自动忽略

对于句子而言，建议您先对句子进行split再使用本项目的metaphone进行处理。  

## <img alt="" src="./doc/readme-image/readme-icon-contribute.png" style="display: inline-block;" width=3%/> 4 参与贡献

本项目由 [SIGCANGJIE / 仓颉兴趣组](https://gitcode.com/SIGCANGJIE) 实现并维护。技术支持和意见反馈请提Issue。

本项目是仓颉兴趣组 [一星级里程碑项目](https://gitcode.com/SIGCANGJIE/homepage/wiki/CompentencyModel.md)。

本项目基于 MIT License，欢迎给我们提交PR，欢迎参与任何形式的贡献。

本项目committer：[@yishengTH](https://gitcode.com/yishengTH)

This project is supervised by [@zhangyin-gitcode](https://gitcode.com/zhangyin_gitcode).